
  var map;
  var users;
  var providers;
  var ajaxMarkers = [];
  var googleMarkers = [];
  var mapIcons = {
    user: '{{ asset("asset/img/marker-user.png") }}',
    active: '{{ asset("asset/img/marker-car.png") }}',
    riding: '{{ asset("asset/img/marker-car.png") }}',
    offline: '{{ asset("asset/img/marker-home.png") }}',
    unactivated: '{{ asset("asset/img/marker-plus.png") }}'
  }

   function initMap() {
    var mapOptions = {
      center: {lat: 0, lng: 0},
      zoom: 3,
      minZoom: 1,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map"),
      mapOptions);

      setInterval(ajaxMapData, 1000);

        var legend = document.getElementById('legend');

        var div = document.createElement('div');
        div.innerHTML = '<img src="' + mapIcons['user'] +  '"> ' + 'Passageiro';
        legend.appendChild(div);

        var div = document.createElement('div');
        div.innerHTML = '<img src="' + mapIcons['offline'] + '"> ' + 'Motorista em Descanso';
        legend.appendChild(div);
        
        var div = document.createElement('div');
        div.innerHTML = '<img src="' + mapIcons['active'] + '"> ' + 'Motorista Disponível';
        legend.appendChild(div);
        
        var div = document.createElement('div');
        div.innerHTML = '<img src="' + mapIcons['unactivated'] + '"> ' + 'Motorista Inativo';
        legend.appendChild(div);
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
        
        google.maps.Map.prototype.clearOverlays = function() {
            for (var i = 0; i < googleMarkers.length; i++ ) {
                googleMarkers[i].setMap(null);
            }
            googleMarkers.length = 0;
        }
    };
    setInterval(ajaxMapData, 1000);
   
 
  function ajaxMapData() {
    map.clearOverlays();
    $.ajax({
      url: '/admin/map/ajax',
      dataType: "JSON",
      headers: {
        'X-CSRF-TOKEN': window.Laravel.csrfToken
      },
      type: "GET",
      success: function(data) {
        console.log('Ajax Response', data);
        ajaxMarkers = data;
      }
    });

    ajaxMarkers ? ajaxMarkers.forEach(addMarkerToMap) : '';
  }

  function addMarkerToMap(element, index) {

    marker = new google.maps.Marker({
      position: {
        lat: element.latitude,
        lng: element.longitude
      },
      id: element.id,
      map: map,
      title: element.first_name + " " + element.last_name + "" + element.mobile,
      icon: mapIcons[element.service ? element.service.status : element.status],
    });

    googleMarkers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
      window.location.href = "tel:element.mobile";
    });
  }
