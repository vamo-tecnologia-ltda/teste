# Vamo Doc de instalação

Release: 27/05/2021 at 02:21am

* Fatores para instalação
- Instale o Xampp 7.4.15 (Caso seu PHP seja maior que 7.4.15)
- Crie um banco de dados no mysql PHPAdmin
- Importe o banco de dados

**No VSCODE**

- abra a pasta do Painel

**Terminal**

- Abra o terminal na pasta do projeto utilizando o VSCODE
- Digite os comandos abaixo:
* composer install --ignore-platform-reqs
* php artisan key:generate
* php artisan storage:link
* php artisan passport:install

Após isso, rode php artisan serve e o painel já irá está no IP informado pelo terminal



**Criado por**
 - VICTOR LIMA
 